package main;

public class Booklist {

    private String title;
    private long idCode;
    private String author;
    private String genre;
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() {
        return title;
    }
    public void setIdCode(Long idCode) {
        this.idCode = idCode;
    }
    public Long getIdCode() {
        return idCode;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getAuthor() {
        return author;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
    public String getGenre() {
        return genre;
    }
    public String toString() {
        return "Title: " + title + " ID Code: " + idCode + " Author: " + author + " Genre " + genre;
    }
}