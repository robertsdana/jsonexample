package main;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSONExample {
    public static String booklistToJson(Booklist book) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(book);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    public static Booklist JsonToBooklist(String s) {
        ObjectMapper mapper = new ObjectMapper();
        Booklist book = null;

        try {
            book = mapper.readValue(s, Booklist.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return book;
    }

    public static void main(String[] args) {
        Booklist book = new Booklist();
        book.setTitle("Nancy Drew and the Hidden Staircase");
        book.setIdCode((long) 123456789);
        book.setAuthor("Carolyn Keene");
        book.setGenre("Mystery");

        String json = JSONExample.booklistToJson(book);
        System.out.println(json);

        Booklist book2 = JSONExample.JsonToBooklist(json);
        System.out.println(book2);
    }
}
